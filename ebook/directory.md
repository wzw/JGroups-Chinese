* 1.[Overview](1.1.md)
 - 1.1. [Channel](1.2.md)
 - 1.2. [Building Blocks](1.3.md)
 - 1.3. [The Protocol Stack](1.4.md)
* 2. [Installation and configuration](2.0.md)
 - 2.1. [Requirements](2.1.md)
 - 2.2. [Structure of the source version](2.1.md)
 - 2.3. [Building JGroups from source](2.3.md)
 - 2.4. [Logging](2.3.md)
  - 2.4.1. [log4j2](2.3.md)
  - 2.4.2. [log4j](2.3.md)
  - 2.4.3. [JDK logging (JUL)](2.3.md)
  - 2.4.4. [Support for custom logging frameworks](2.3.md)
 - 2.5. [Testing your setup](2.3.md)
 - 2.6. [Running a demo program](2.3.md)
 - 2.7. [Using IP Multicasting without a network connection](2.3.md)
 - 2.8. [It doesn��t work !](2.3.md)
  - 2.8.1. [mcast](2.3.md)
 - 2.9. [Problems with IPv6](2.3.md)
 - 2.10. [Wiki](2.3.md)
 - 2.11. [I have discovered a bug !](2.3.md)
 - 2.12. [Supported classes](2.3.md)
  - 2.12.1. [Experimental classes](2.3.md)
  - 2.12.2. [Unsupported classes](2.3.md)
* 3. [API](3.0.md)
 - 3.1. [Utility classes](3.0.md)
  - 3.1.1. [objectToByteBuffer(), objectFromByteBuffer()](3.0.md)
  - 3.1.2. [objectToStream(), objectFromStream()](3.0.md)
 - 3.2. [Interfaces](3.0.md)
  - 3.2.1. [MessageListener](3.0.md)
  - 3.2.2. [MembershipListener](3.0.md)
  - 3.2.3. [Receiver](3.0.md)
  - 3.2.4. [ReceiverAdapter](3.0.md)
  - 3.2.5. [ChannelListener](3.0.md)
 - 3.3. [Address](3.0.md)
 - 3.4. [Message](3.0.md)
 - 3.5. [Header](3.0.md)
 - 3.6. [Event](3.0.md)
 - 3.7. [View](3.0.md) 
  - 3.7.1. [ViewId](3.0.md)
  - 3.7.2. [MergeView](3.0.md)
 - 3.8. [JChannel](3.0.md)   
  - 3.8.1. [Creating a channel](3.0.md)
  - 3.8.2. [Giving the channel a logical name](3.0.md)  
  - 3.8.3. [Generating custom addresses](3.0.md)  
  - 3.8.4. [Joining a cluster](3.0.md)  
  - 3.8.5. [Joining a cluster and getting the state in one operation](3.0.md)  
  - 3.8.6. [Getting the local address and the cluster name](3.0.md)  
  - 3.8.7. [Getting the current view](3.0.md)  
  - 3.8.8. [Sending messages](3.0.md)  
  - 3.8.9. [Receiving messages](3.0.md)  
  - 3.8.10. [Receiving view changes](3.0.md)  
  - 3.8.11. [Getting the group��s state](3.0.md)  
  - 3.8.12. [Disconnecting from a channel](3.0.md)
  - 3.8.13. [Closing a channel](3.0.md)    
  
  
  
  